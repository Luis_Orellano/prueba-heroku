import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DateViewComponent } from './date-view/date-view.component';

const routes: Routes = [
  {path: 'date', component: DateViewComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
