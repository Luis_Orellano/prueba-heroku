import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { DateService } from 'src/services/date.service';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { DateViewComponent } from './date-view/date-view.component';

@NgModule({
  declarations: [AppComponent, DateViewComponent],
  imports: [BrowserModule, AppRoutingModule],
  providers: [DateService],
  bootstrap: [AppComponent],
})
export class AppModule {}
