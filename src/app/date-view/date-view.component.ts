import { Time } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DateService } from 'src/services/date.service';

@Component({
  selector: 'date-view',
  templateUrl: './date-view.component.html',
  styleUrls: ['./date-view.component.scss'],
})
export class DateViewComponent implements OnInit {
  @ViewChild('Dateh1', { static: true })
  dateh1!: ElementRef;
  date!: Date;
  flag: Date = new Date(0, 0, 0, 0, 15, 0, 0);

  constructor(private _dateService: DateService) {
    this.actualizarHoraAutomatico();
  }

  ngOnInit(): void {
    console.log('holi');
  }

  actualizarHora() {
    console.log(this.dateh1);
    let stringDate: string[] = this._dateService
      .getDate()
      .toLocaleTimeString()
      .split(':', 3);
    console.log(stringDate);
    console.log(this._dateService.getDate());
  }

  actualizarHoraAutomatico() {
    setInterval(() => {
      this.date = this._dateService.getDate();
      this.checkHour();
    }, 1000);
  }

  checkHour() {
    if (this.date.getMinutes().toString() === '48') {
      return console.log('holi');
    } else {
      return console.log(this.date.getHours());
    }
  }
}
