import { Injectable } from "@angular/core";

@Injectable()
export class DateService {
  public getDate(): Date {
    return new Date();
  }
}
